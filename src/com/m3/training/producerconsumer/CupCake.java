package com.m3.training.producerconsumer;

public class CupCake {

	private String name;
	
	public CupCake() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
