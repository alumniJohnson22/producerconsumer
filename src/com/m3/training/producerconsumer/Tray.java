package com.m3.training.producerconsumer;

import java.util.ArrayList;

public class Tray {
	
	private static final int FIRST_CUPCAKE = 0;
	private static final int TOTAL_CUPCAKES = 1;
	
	private ArrayList<CupCake> tray;

	public Tray() {
		tray = new ArrayList<>();
	}

	public CupCake removeCupCake() {
		CupCake cupCake = null;
		InterruptedException savedException = null;
		while (tray.isEmpty()) {
			synchronized(tray) {
				if (tray.isEmpty()) {
					try {
						tray.wait();
					} catch (InterruptedException e) {
						savedException = e;
						break;
					}
				}
				cupCake = tray.remove(FIRST_CUPCAKE);
			}
		}
		
		if (cupCake == null && savedException != null) {
			String msg = "No cupcake could be removed due to exception: " + savedException.getMessage();
			throw new IllegalStateException(msg, savedException);
		}
		else if (cupCake == null) {
			String msg = "No cupcake could be removed due an unknown problem.";
			throw new IllegalStateException(msg);		
		}
		
		return cupCake;
	}
	
	public CupCake addCupCake() {
		CupCake cupCake = null;
		InterruptedException savedException = null;
		
		while (tray.size() == TOTAL_CUPCAKES) {
			synchronized(tray) {
				if ((tray.size() == TOTAL_CUPCAKES) ) {
					try {
						tray.wait();
					} catch (InterruptedException e) {
						savedException = e;
						break;
					}
				}
				cupCake = new CupCake();
				cupCake.setName("Delicious Cupcake");
				if (!tray.add(cupCake)) {
					cupCake = null;
				}
			}
		}
		
		if (cupCake == null && savedException != null) {
			String msg = "No cupcake could be added due to exception: " + savedException.getMessage();
			throw new IllegalStateException(msg, savedException);
		}
		else if (cupCake == null) {
			String msg = "No cupcake could be added due an unknown problem.";
			throw new IllegalStateException(msg);		
		}
		
		return cupCake;
	}


}
